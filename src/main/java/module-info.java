module com.search.files.searchfiles {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.kordamp.ikonli.javafx;
    requires org.kordamp.bootstrapfx.core;
    requires MaterialFX;
    requires static lombok;
    requires java.sql;
    requires FindAllFile;
    requires org.apache.logging.log4j;

    exports com.search.files.searchfiles;
    exports com.search.files.searchfiles.controller;
    exports com.search.files.searchfiles.data.entity;
    exports com.search.files.searchfiles.data.model;
    exports com.search.files.searchfiles.service;

    opens com.search.files.searchfiles to javafx.fxml, lombok, MaterialFX;
    opens com.search.files.searchfiles.controller to javafx.fxml, MaterialFX, lombok;
    opens com.search.files.searchfiles.data.entity to lombok;
    opens com.search.files.searchfiles.data.model to lombok;
    opens com.search.files.searchfiles.service to java.sql;
}