package com.search.files.searchfiles.data.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.Objects;

@Builder
@Getter
@Setter
public final class DirectoriesModel
{
    private long id;
    private String path;
    private String createdAt;
    private String lastSearchAt;
    private boolean deleted;
    private String deletedAt;

    @Override
    public String toString()
    {
        return path;
    }
}
