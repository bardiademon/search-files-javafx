package com.search.files.searchfiles.data.entity;

import lombok.Builder;

import java.time.LocalDateTime;

@Builder
public record DirectoriesEntity(long id , String path , LocalDateTime createdAt ,
                                LocalDateTime lastSearchAt , boolean deleted ,
                                LocalDateTime deletedAt)
{
}
