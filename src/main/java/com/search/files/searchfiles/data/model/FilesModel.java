package com.search.files.searchfiles.data.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Builder
@Getter
@Setter
@ToString
public final class FilesModel
{
    private long index;
    private long id;
    private String filename;
    private String path;
    private String description;
    private String updatedAt;
    private String createdAt;
    private boolean deleted;
    private String deletedAt;
    private String contentType;
    private String fileExtension;
    private long fileSize;
}
