package com.search.files.searchfiles.data.mapper;

import com.search.files.searchfiles.data.entity.DirectoriesEntity;
import com.search.files.searchfiles.data.model.DirectoriesModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public final class DirectoriesMapper
{
    private final static Logger logger = LogManager.getLogger(DirectoriesMapper.class);

    private DirectoriesMapper()
    {
    }

    public static DirectoriesEntity toDirectoryEntity(final ResultSet resultSet) throws SQLException
    {
        logger.info("toDirectoryEntity starting...");

        if (resultSet != null && (resultSet.last() && resultSet.getRow() > 0) && resultSet.first())
        {
            final Timestamp createdAt = resultSet.getTimestamp("created_at");
            final Timestamp lastSearchAt = resultSet.getTimestamp("last_search_at");
            final Timestamp deletedAt = resultSet.getTimestamp("deleted_at");

            return new DirectoriesEntity(
                    resultSet.getLong("id") ,
                    resultSet.getString("path") ,
                    createdAt == null ? null : createdAt.toLocalDateTime() ,
                    lastSearchAt == null ? null : lastSearchAt.toLocalDateTime() ,
                    resultSet.getBoolean("deleted") ,
                    deletedAt == null ? null : deletedAt.toLocalDateTime()
            );
        }

        return null;
    }

    public static List<DirectoriesEntity> toDirectoriesEntity(final ResultSet resultSet) throws SQLException
    {
        logger.info("toDirectoriesEntity starting...");

        if (resultSet != null && (resultSet.last() && resultSet.getRow() > 0) && resultSet.first())
        {
            final List<DirectoriesEntity> directories = new ArrayList<>();

            do
            {
                final Timestamp createdAt = resultSet.getTimestamp("created_at");
                final Timestamp lastSearchAt = resultSet.getTimestamp("last_search_at");
                final Timestamp deletedAt = resultSet.getTimestamp("deleted_at");

                directories.add(new DirectoriesEntity(
                        resultSet.getLong("id") ,
                        resultSet.getString("path") ,
                        createdAt == null ? null : createdAt.toLocalDateTime() ,
                        lastSearchAt == null ? null : lastSearchAt.toLocalDateTime() ,
                        resultSet.getBoolean("deleted") ,
                        deletedAt == null ? null : deletedAt.toLocalDateTime()
                ));
            }
            while (resultSet.next());

            return directories;
        }

        return null;
    }

    public static File[] toFilesArray(final List<DirectoriesEntity> directoriesEntity)
    {
        logger.info("toFilesArray starting... {}" , directoriesEntity);

        if (directoriesEntity != null && directoriesEntity.size() > 0)
        {
            final File[] directoriesModel = new File[directoriesEntity.size()];

            for (int i = 0; i < directoriesEntity.size(); i++)
                directoriesModel[i] = new File(directoriesEntity.get(i).path());

            return directoriesModel;
        }

        return null;
    }

    public static List<DirectoriesModel> toDirectoriesModel(final List<DirectoriesEntity> directoriesEntity)
    {
        logger.info("toDirectoriesModel starting... {}" , directoriesEntity);

        final List<DirectoriesModel> directoriesModel = new ArrayList<>();

        if (directoriesEntity != null && directoriesEntity.size() > 0)
        {
            for (final DirectoriesEntity item : directoriesEntity)
            {
                final String lastSearchAt = item.lastSearchAt() == null ? null : item.lastSearchAt().toString();
                final String deletedAt = item.deletedAt() == null ? null : item.deletedAt().toString();

                final DirectoriesModel model = DirectoriesModel.builder()
                        .id(item.id())
                        .path(item.path())
                        .createdAt(item.createdAt().toString())
                        .deleted(item.deleted())
                        .lastSearchAt(lastSearchAt)
                        .deletedAt(deletedAt)
                        .build();

                directoriesModel.add(model);
            }
        }

        logger.info("toDirectoriesModel finished: {}" , directoriesModel);

        return directoriesModel;
    }

    public static Long getId(final ResultSet resultSet) throws SQLException
    {
        logger.info("getId starting...");

        if (resultSet != null && (resultSet.last() && resultSet.getRow() > 0) && resultSet.first())
        {
            return resultSet.getLong("id");
        }

        return null;
    }
}
