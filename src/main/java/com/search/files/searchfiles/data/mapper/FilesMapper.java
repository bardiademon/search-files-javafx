package com.search.files.searchfiles.data.mapper;

import com.search.files.searchfiles.data.entity.FilesEntity;
import com.search.files.searchfiles.data.model.FilesModel;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public final class FilesMapper
{
    private final static Logger logger = LogManager.getLogger(FilesMapper.class);

    private FilesMapper()
    {
    }


    public static List<FilesEntity> toFilesEntity(final List<FilesModel> filesModels)
    {
        final List<FilesEntity> filesEntity = new ArrayList<>();

        if (filesModels != null && filesModels.size() > 0)
        {
            for (final FilesModel filesModel : filesModels)
            {
                filesEntity.add(toFileEntity(filesModel , 0));
            }
        }

        return filesEntity;
    }

    public static List<FilesEntity> toFilesEntity(final ResultSet resultSet) throws SQLException
    {
        final List<FilesEntity> filesEntity = new ArrayList<>();

        if (resultSet != null && (resultSet.last() && resultSet.getRow() > 0) && resultSet.first())
        {
            do
            {
                final Timestamp updatedAtTimestamp = resultSet.getTimestamp("updated_at");
                LocalDateTime updatedAt = updatedAtTimestamp == null ? null : updatedAtTimestamp.toLocalDateTime();

                final Timestamp deletedAtTimestamp = resultSet.getTimestamp("deleted_at");
                final LocalDateTime deletedAt = deletedAtTimestamp == null ? null : deletedAtTimestamp.toLocalDateTime();

                filesEntity.add(
                        new FilesEntity(
                                resultSet.getLong("id") ,
                                resultSet.getString("filename") ,
                                resultSet.getString("path") ,
                                resultSet.getString("description") ,
                                updatedAt ,
                                resultSet.getTimestamp("created_at").toLocalDateTime() ,
                                resultSet.getBoolean("deleted") ,
                                deletedAt ,
                                resultSet.getLong("main_directory_id")
                        ));
            }
            while (resultSet.next());

        }

        return filesEntity;
    }

    public static FilesEntity toFileEntity(final ResultSet resultSet) throws SQLException
    {
        if (resultSet != null && (resultSet.last() && resultSet.getRow() > 0) && resultSet.first())
        {
            final Timestamp updatedAtTimestamp = resultSet.getTimestamp("updated_at");
            LocalDateTime updatedAt = updatedAtTimestamp == null ? null : updatedAtTimestamp.toLocalDateTime();

            final Timestamp deletedAtTimestamp = resultSet.getTimestamp("deleted_at");
            final LocalDateTime deletedAt = deletedAtTimestamp == null ? null : deletedAtTimestamp.toLocalDateTime();

            return new FilesEntity(
                    resultSet.getLong("id") ,
                    resultSet.getString("filename") ,
                    resultSet.getString("path") ,
                    resultSet.getString("description") ,
                    updatedAt ,
                    resultSet.getTimestamp("created_at").toLocalDateTime() ,
                    resultSet.getBoolean("deleted") ,
                    deletedAt ,
                    resultSet.getLong("main_directory_id"));
        }

        return null;
    }

    public static FilesEntity toFileEntity(final FilesModel fileModel , final long mainDirectoryId)
    {
        logger.info("Converting => FilesMode: {} to FilesEntity" , fileModel);
        return new FilesEntity(
                fileModel.getId() ,
                fileModel.getFilename() ,
                fileModel.getPath() ,
                fileModel.getDescription() ,
                fileModel.getUpdatedAt() == null ? null : LocalDateTime.parse(fileModel.getUpdatedAt()) ,
                fileModel.getCreatedAt() == null ? null : LocalDateTime.parse(fileModel.getCreatedAt()) ,
                fileModel.isDeleted() ,
                fileModel.getDeletedAt() == null ? null : LocalDateTime.parse(fileModel.getDeletedAt()) ,
                mainDirectoryId
        );
    }

    public static List<FilesModel> toFilesModels(final List<FilesEntity> filesEntity)
    {
        final List<FilesModel> filesModels = new ArrayList<>();

        if (filesEntity != null && filesEntity.size() > 0)
        {
            for (int i = 0, len = filesEntity.size(); i < len; i++)
                filesModels.add(toFileModel(filesEntity.get(i) , i));
        }

        return filesModels;
    }

    public static FilesModel toFileModel(final FilesEntity fileEntity , final int index)
    {
        if (fileEntity != null)
        {
            final String updatedAt = fileEntity.updatedAt() == null ? null : fileEntity.updatedAt().toString();
            final String deletedAt = fileEntity.deletedAt() == null ? null : fileEntity.deletedAt().toString();

            final File fileItem = new File(fileEntity.path());

            long fileSize = 0;
            String fileExtension = null;
            boolean deleted = fileEntity.deleted();

            if (fileItem.exists())
            {
                fileSize = fileItem.length();
                fileExtension = FilenameUtils.getExtension(fileItem.getPath());
            }
            else
            {
                deleted = true;
            }

            return FilesModel.builder()
                    .index(index)
                    .id(fileEntity.id())
                    .filename(fileEntity.filename())
                    .path(fileEntity.path())
                    .fileExtension(fileExtension)
                    .fileSize(fileSize)
                    .createdAt(fileEntity.createdAt().toString())
                    .description(fileEntity.description())
                    .updatedAt(updatedAt)
                    .deletedAt(deletedAt)
                    .deleted(deleted)
                    .build();

        }

        return null;
    }
}
