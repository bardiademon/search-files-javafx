package com.search.files.searchfiles.data.entity;

import lombok.Builder;

import java.time.LocalDateTime;

@Builder
public record FilesEntity(long id , String filename , String path , String description ,
                          LocalDateTime updatedAt , LocalDateTime createdAt , boolean deleted ,
                          LocalDateTime deletedAt , long mainDirectoryId)
{
}
