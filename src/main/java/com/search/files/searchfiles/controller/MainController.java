package com.search.files.searchfiles.controller;

import bardiademon.FindAllFile;
import com.search.files.searchfiles.SearchFilesApplication;
import com.search.files.searchfiles.data.entity.DirectoriesEntity;
import com.search.files.searchfiles.data.entity.FilesEntity;
import com.search.files.searchfiles.data.mapper.DirectoriesMapper;
import com.search.files.searchfiles.data.mapper.FilesMapper;
import com.search.files.searchfiles.data.model.DirectoriesModel;
import com.search.files.searchfiles.data.model.FilesModel;
import com.search.files.searchfiles.service.DirectoriesService;
import com.search.files.searchfiles.service.FilesService;
import io.github.palexdev.materialfx.controls.*;
import io.github.palexdev.materialfx.controls.cell.MFXTableRowCell;
import io.github.palexdev.materialfx.filter.LongFilter;
import io.github.palexdev.materialfx.filter.StringFilter;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Function;

public final class MainController implements Initializable
{
    private final static Logger logger = LogManager.getLogger(MainController.class);

    private static final String BTN_SCAN_TEXT = "Scan Directories";

    @FXML
    private MFXTableView<FilesModel> tblFilesResultSelected;

    @FXML
    private MFXListView<DirectoriesModel> directories;

    @FXML
    private MFXButton btnScanDirectories;

    @FXML
    private MFXTextField txtSearch;

    private File lastDirectoryChooser;

    @Override
    public void initialize(final URL url , final ResourceBundle resourceBundle)
    {
        setTableColumns();
    }

    private void setTableColumns()
    {
        Platform.runLater(() ->
        {
            final ObservableList<MFXTableColumn<FilesModel>> tableColumns = tblFilesResultSelected.getTableColumns();

            final MFXTableColumn<FilesModel> indexColumn = getTableColumn("#" , Comparator.comparing(FilesModel::getIndex));
            indexColumn.setRowCellFactory(fileModel -> getTableRowCell(FilesModel::getIndex));
            tableColumns.add(indexColumn);
            logger.info("Added table column index");

            final MFXTableColumn<FilesModel> idColumn = getTableColumn("Id" , Comparator.comparing(FilesModel::getId));
            idColumn.setRowCellFactory(fileModel -> getTableRowCell(FilesModel::getId));
            tableColumns.add(idColumn);
            logger.info("Added table column id");

            final MFXTableColumn<FilesModel> filenameColumn = getTableColumn("Filename" , Comparator.comparing(FilesModel::getFilename));
            filenameColumn.setRowCellFactory(fileModel -> getTableRowCell(FilesModel::getFilename));
            tableColumns.add(filenameColumn);
            logger.info("Added table column Filename");

            final MFXTableColumn<FilesModel> fileExtensionColumn = getTableColumn("File extension" , Comparator.comparing(FilesModel::getFilename));
            fileExtensionColumn.setRowCellFactory(fileModel -> getTableRowCell(FilesModel::getFileExtension));
            tableColumns.add(fileExtensionColumn);
            logger.info("Added table column FileExtension");

            final MFXTableColumn<FilesModel> filesizeColumn = getTableColumn("Filesize" , Comparator.comparing(FilesModel::getFilename));
            filesizeColumn.setRowCellFactory(fileModel -> getTableRowCell(FilesModel::getFileSize));
            tableColumns.add(filesizeColumn);
            logger.info("Added table column Filesize");

            final MFXTableColumn<FilesModel> pathColumn = getTableColumn("Path" , Comparator.comparing(FilesModel::getPath));
            pathColumn.setRowCellFactory(fileModel -> getTableRowCell(FilesModel::getPath));
            tableColumns.add(pathColumn);
            logger.info("Added table column Path");

            final MFXTableColumn<FilesModel> descriptionColumn = getTableColumn("Description" , Comparator.comparing(FilesModel::getDescription));
            descriptionColumn.setRowCellFactory(fileModel -> getTableRowCell(FilesModel::getDescription));
            tableColumns.add(descriptionColumn);
            logger.info("Added table column Description");

            final MFXTableColumn<FilesModel> updatedAtColumn = getTableColumn("Updated at" , Comparator.comparing(FilesModel::getUpdatedAt));
            updatedAtColumn.setRowCellFactory(fileModel -> getTableRowCell(FilesModel::getUpdatedAt));
            tableColumns.add(updatedAtColumn);
            logger.info("Added table column Updated at");

            final MFXTableColumn<FilesModel> createdAtColumn = getTableColumn("Created at" , Comparator.comparing(FilesModel::getCreatedAt));
            createdAtColumn.setRowCellFactory(fileModel -> getTableRowCell(FilesModel::getCreatedAt));
            tableColumns.add(createdAtColumn);
            logger.info("Added table column Created at");


            tblFilesResultSelected.getFilters().addAll(
                    new LongFilter<>("#" , FilesModel::getIndex) ,
                    new LongFilter<>("Id" , FilesModel::getId) ,
                    new StringFilter<>("Filename" , FilesModel::getFilename) ,
                    new LongFilter<>("Filesize" , FilesModel::getFileSize) ,
                    new StringFilter<>("file extension" , FilesModel::getFileExtension) ,
                    new StringFilter<>("description" , FilesModel::getDescription)
            );
        });
    }

    private MFXTableColumn<FilesModel> getTableColumn(final String name , final Comparator<FilesModel> comparator)
    {
        return new MFXTableColumn<>(name , true , comparator);
    }

    private MFXTableRowCell<FilesModel, ?> getTableRowCell(final Function<FilesModel, ?> extractor)
    {
        final MFXTableRowCell<FilesModel, ?> tableRowCell = new MFXTableRowCell<>(extractor);
        tableRowCell.setOnMouseClicked(e -> onClickTblFiles());
        return tableRowCell;
    }


    @FXML
    public void onClickBtnScanDirectories()
    {
        setDirectories();
    }

    @FXML
    public void onClickBtnAddDirectory()
    {
        Platform.runLater(() ->
        {
            final DirectoryChooser directoryChooser = new DirectoryChooser();
            directoryChooser.setInitialDirectory(lastDirectoryChooser);
            directoryChooser.setTitle("Please select directory");
            lastDirectoryChooser = directoryChooser.showDialog(null);

            logger.info("Selected file: {}" , lastDirectoryChooser);

            if (lastDirectoryChooser != null)
            {
                final DirectoriesEntity byPath = DirectoriesService.findByPath(lastDirectoryChooser.getPath());

                if (byPath == null)
                {
                    final boolean add = DirectoriesService.add(lastDirectoryChooser.getPath());
                    if (add)
                    {
                        alert(Alert.AlertType.INFORMATION , "Successfully" , "Added new path successfully" , lastDirectoryChooser.getPath());
                    }
                    else
                    {
                        alert(Alert.AlertType.ERROR , "Error" , "Error add new path" , lastDirectoryChooser.getPath());
                    }
                }
                else
                {
                    alert(Alert.AlertType.ERROR , "Error" , "This path is exists" , lastDirectoryChooser.getPath());
                }
            }
        });
    }

    public static void alert(final Alert.AlertType type , final String title , final String header , final String content)
    {
        Platform.runLater(() ->
        {
            final Alert alert = new Alert(type);
            alert.setTitle(title);
            alert.setHeaderText(header);
            alert.setContentText(content);
            alert.show();
        });
    }

    private void setDirectories()
    {
        DirectoriesService.findAll(directories -> Platform.runLater(() ->
        {
            MainController.this.directories.getItems().clear();
            MainController.this.directories.getItems().addAll(DirectoriesMapper.toDirectoriesModel(directories));

            scanDirectories(directories);
        }));
    }

    private void scanDirectories(final List<DirectoriesEntity> directoriesEntity)
    {
        tblFilesResultSelected.getItems().clear();

        final File[] directories = DirectoriesMapper.toFilesArray(directoriesEntity);
        if (directories != null && directories.length > 0)
        {
            final long[] counter = {0};
            startScanning();
            new FindAllFile(new FindAllFile.CallBack()
            {
                @Override
                public void FindTimeFile(long l , FindAllFile.Find find)
                {
                    logger.info("Scanning: " + find.file);
                    addFileToList(find , ++counter[0]);
                }

                @Override
                public void FindTimeDir(long l , File file)
                {

                }

                @Override
                public void FindTimeFileOrDir(long l , long l1 , String s , FindAllFile.Find find , File file)
                {
                }

                @Override
                public void AfterFindFile(long l , List<FindAllFile.Find> list)
                {
                    logger.info("Scanned | Number of files: {}" , l);
                    stopScanning();
                }

                @Override
                public void AfterFindDir(long l , List<File> list)
                {
                }

                @Override
                public boolean Error(String s)
                {
                    return false;
                }
            } , directories);
        }
    }

    private void startScanning()
    {
        Platform.runLater(() ->
        {
            btnScanDirectories.setText(BTN_SCAN_TEXT + " ||| Scanning...");
            btnScanDirectories.setDisable(true);
        });
    }

    private void stopScanning()
    {
        Platform.runLater(() ->
        {
            btnScanDirectories.setText(BTN_SCAN_TEXT);
            btnScanDirectories.setDisable(false);
        });
        addFilesToDb();
    }

    private long getDirectoryId(final String filePath)
    {
        logger.info("getDirectoryId by path: {}" , filePath);
        for (final DirectoriesModel item : directories.getItems())
        {
            final boolean startWith = filePath.startsWith(item.getPath());
            if (startWith) return item.getId();
        }

        return 0;
    }

    @FXML
    public void onTxtSearchTextChanged()
    {
        logger.info("onTxtSearchTextChanged starting...");
        Platform.runLater(() ->
        {
            final String filename = txtSearch.getText();
            if (filename == null || filename.isEmpty())
            {
                logger.info("filename is empty");

                logger.info("Getting all files");
                FilesService.findAllFile(filesEntity ->
                {
                    final List<FilesModel> filesModels = FilesMapper.toFilesModels(filesEntity);

                    logger.info("Checking exists file: {}" , filesModels.size());
                    boolean remove = false;
                    for (final FilesModel filesModel : filesModels)
                    {
                        final String itemPath = filesModel.getPath();

                        logger.info("Checking exists file: {}" , itemPath);
                        if (!new File(itemPath).exists())
                        {
                            if (!remove) remove = true;

                            logger.warn("File not exists: {}" , itemPath);

                            logger.info("Removing file from database: {}" , itemPath);
                            if (FilesService.deleteFileById(filesModel.getId()))
                            {
                                logger.info("File removed from database: {}" , itemPath);
                            }
                            else
                            {
                                logger.error("Fail to remove file from database: {}" , itemPath);
                            }
                        }
                        else logger.info("File is exists: {}" , itemPath);
                    }

                    if (remove)
                    {
                        onTxtSearchTextChanged();
                        return;
                    }

                    logger.info("Clear table files");
                    tblFilesResultSelected.getItems().clear();

                    logger.info("Adding {} files to table files" , filesModels.size());
                    tblFilesResultSelected.getItems().addAll(filesModels);
                });
            }
            else
            {
                logger.info("Finding filename: {}" , filename);
                FilesService.findFileByFilename(filename , filesEntity ->
                {
                    final List<FilesModel> filesModels = FilesMapper.toFilesModels(filesEntity);

                    tblFilesResultSelected.getItems().clear();

                    if (filesModels.size() > 0)
                    {
                        logger.info("Finding filename: {} , Size: {}" , filename , filesModels.size());

                        logger.info("Adding {} files to table files" , filesModels.size());
                        tblFilesResultSelected.getItems().addAll(filesModels);
                    }

                });
            }
        });
    }

    private void addFileToList(final FindAllFile.Find find , final long index)
    {
        Platform.runLater(() ->
        {
            final FilesModel model = FilesModel.builder()
                    .index(index)
                    .filename(find.nameAndType)
                    .path(find.file.getPath())
                    .fileExtension(find.typeFile)
                    .fileSize(find.size)
                    .createdAt(LocalDateTime.now().toString())
                    .build();

            tblFilesResultSelected.getItems().add(model);
        });
    }

    private void addFilesToDb()
    {
        Platform.runLater(() ->
        {
            final List<FilesModel> filesModels = new ArrayList<>(tblFilesResultSelected.getItems().stream().toList());

            int counter = filesModels.size();

            final List<FilesModel> newFilesModels = new ArrayList<>();
            for (int i = 0, len = filesModels.size(); i < len; i++)
            {
                final FilesModel fileModel = filesModels.get(i);

                final String fileModelPath = fileModel.getPath();

                logger.info("Find file: {}" , fileModelPath);
                final FilesEntity fileByPath = FilesService.findFileByPath(fileModelPath);
                if (fileByPath == null)
                {
                    final FilesEntity fileEntity = FilesMapper.toFileEntity(fileModel , getDirectoryId(fileModelPath));

                    logger.info("Adding file: {}" , fileModelPath);

                    final long addFile = FilesService.addFile(fileEntity);
                    if (addFile > 0)
                    {
                        logger.info("Added file: {}" , fileModelPath);
                        final FilesModel model = FilesMapper.toFileModel(fileEntity , counter++);
                        model.setId(addFile);

                        newFilesModels.add(model);
                    }
                    else
                    {
                        logger.error("Fail to add file: {}" , fileModelPath);
                    }
                }
                else
                {
                    fileModel.setId(fileByPath.id());
                    filesModels.set(i , fileModel);
                }
            }

            if (newFilesModels.size() > 0)
            {
                filesModels.addAll(newFilesModels);
                newFilesModels.clear();
            }
            addToTableFiles(filesModels);

        });
    }

    private void addToTableFiles(final List<FilesModel> filesModels)
    {
        logger.info("addToTableFiles starting...");
        Platform.runLater(() ->
        {
            logger.info("Clear table files");
            tblFilesResultSelected.getItems().clear();

            logger.info("Adding {} files to table files" , filesModels.size());
            tblFilesResultSelected.getItems().addAll(filesModels);
        });
    }

    public void onClickTblFiles()
    {
        if (tblFilesResultSelected.getItems() != null && tblFilesResultSelected.getItems().size() > 0)
        {
            final List<FilesModel> selectedValues = tblFilesResultSelected.getSelectionModel().getSelectedValues();
            if (selectedValues != null && selectedValues.size() > 0)
            {
                final FilesModel model = selectedValues.get(0);
                selectedFileItem(model);
            }
        }
    }

    private void selectedFileItem(final FilesModel model)
    {
        logger.info("selectedFileItem starting... {}" , model);
        Platform.runLater(() ->
        {
            try
            {
                logger.info("Load item stage");
                final FXMLLoader fxmlLoader = new FXMLLoader(SearchFilesApplication.class.getResource("item.fxml"));
                final Scene scene = new Scene(fxmlLoader.load());

                final ItemController itemController = fxmlLoader.getController();
                itemController.setValue(model);

                final Stage stage = new Stage();
                stage.setTitle("Item");
                stage.setScene(scene);
                stage.getScene().getWindow().addEventFilter(WindowEvent.WINDOW_CLOSE_REQUEST , windowEvent -> itemController.onClickBtnClose());
                stage.show();

                itemController.setStage(stage);

                logger.info("Loaded item stage");
            }
            catch (IOException e)
            {
                logger.info("Fail to load item stage" , e);
            }

        });
    }

    public void onClickLstDirectories()
    {
        logger.info("On item list directories clicked");
        Platform.runLater(() ->
        {
            final ObservableMap<Integer, DirectoriesModel> selection = directories.getSelectionModel().getSelection();
            if (selection != null && selection.size() == 1)
            {
                final Integer key = selection.keySet().stream().toList().get(0);

                final DirectoriesModel model = selection.get(key);
                logger.info("Directory selected: {}" , model);

                logger.info("Showing question delete directory: {}" , model);

                final String directoryPath = model.getPath();

                final Alert alert = new Alert(Alert.AlertType.CONFIRMATION ,
                        String.format("Are you want to deleted this directory from database? {%s} " ,
                                FilenameUtils.getName(directoryPath)) ,
                        ButtonType.YES , ButtonType.CANCEL);

                final Optional<ButtonType> buttonTypeOptional = alert.showAndWait();
                if (buttonTypeOptional.isPresent())
                {
                    final ButtonType buttonType = buttonTypeOptional.get();
                    logger.info("Directory delete: {}" , buttonType.toString());

                    if (buttonType.equals(ButtonType.YES))
                    {
                        if (DirectoriesService.deleteById(model.getId()))
                        {
                            alert(Alert.AlertType.INFORMATION , "Delete directory" , "Successfully to delete directory: " + FilenameUtils.getName(directoryPath) , null);

                            logger.info("Successfully to delete directory: {}" , model);

                            logger.info("Remove directory from list");
                            directories.getItems().remove((int) key);

                            logger.info("Delete files from main directory {}" , directoryPath);
                            FilesService.deleteFileByMainDirectory(model.getId() , removed ->
                            {
                                if (removed)
                                {
                                    logger.info("Deleted files by main directory: {}" , directoryPath);
                                    onTxtSearchTextChanged();
                                }
                                else
                                {
                                    logger.error("Fail to deleted files by main directory: {}" , directoryPath);
                                }
                            });
                        }
                        else
                        {
                            logger.info("Fail to delete directory: {}" , model);
                            alert(Alert.AlertType.ERROR , "Delete directory" , "Fail to delete directory: " + FilenameUtils.getName(directoryPath) , "Please check database connection");
                        }
                    }
                }
            }
        });
    }
}