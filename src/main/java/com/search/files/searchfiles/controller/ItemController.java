package com.search.files.searchfiles.controller;

import com.search.files.searchfiles.data.entity.FilesEntity;
import com.search.files.searchfiles.data.mapper.FilesMapper;
import com.search.files.searchfiles.data.model.FilesModel;
import com.search.files.searchfiles.service.FilesService;
import com.search.files.searchfiles.service.Result;
import io.github.palexdev.materialfx.controls.MFXTextField;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;

import static com.search.files.searchfiles.controller.MainController.alert;

public final class ItemController implements Initializable
{
    private final static Logger logger = LogManager.getLogger(ItemController.class);

    @FXML
    public Label lblExtension;

    @FXML
    public TextArea txtDescription;

    @FXML
    private MFXTextField txtFilename;

    @FXML
    private MFXTextField txtPath;

    @FXML
    private Label lblFilesize;

    @FXML
    private Label lblUpdatedAt;

    @FXML
    private Label lblCreatedAt;

    @FXML
    private Label lblFileExtension;

    private FilesModel fileModel;

    private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm");

    private Stage stage;

    @Override
    public void initialize(final URL url , final ResourceBundle resourceBundle)
    {
        logger.info("Item controller starting...");
    }

    public void setStage(final Stage stage)
    {
        this.stage = stage;
    }

    public void setValue(final FilesModel fileModel)
    {
        this.fileModel = fileModel;

        Platform.runLater(() ->
        {
            logger.info("Set value: {}" , fileModel);

            try
            {
                final String createdAt = LocalDateTime.parse(fileModel.getCreatedAt()).format(dateTimeFormatter);
                final String updatedAt = fileModel.getUpdatedAt() == null ? null : LocalDateTime.parse(fileModel.getUpdatedAt()).format(dateTimeFormatter);

                txtFilename.setText(fileModel.getFilename());
                txtPath.setText(fileModel.getPath());
                txtDescription.setText(fileModel.getDescription());
                lblExtension.setText(fileModel.getFileExtension().toUpperCase(Locale.ROOT));
                lblFileExtension.setText(fileModel.getFileExtension());
                lblFilesize.setText(String.valueOf(fileModel.getFileSize()));
                lblCreatedAt.setText(createdAt);
                lblUpdatedAt.setText(updatedAt);

                logger.info("Set value successfully: {}" , fileModel);
            }
            catch (Exception e)
            {
                logger.info("Set value fail: {}" , fileModel , e);
            }
        });
    }

    @FXML
    private void onClickBtnUpdate()
    {
        logger.info("onClickBtnUpdate starting...");
        question("Are you want to update this item?" , yesClick ->
        {
            final String filename = txtFilename.getText();
            renameFile(fileModel.getPath() , filename , renamed ->
            {
                fileModel.setDescription(txtDescription.getText());
                fileModel.setUpdatedAt(LocalDateTime.now().toString());
                fileModel.setFilename(filename);

                final File newPath = new File(new File(fileModel.getPath()).getParent() + File.separator + filename);
                fileModel.setPath(newPath.getPath());

                update();
            });
        });
    }

    private void renameFile(final String path , final String newFilename , final Result<?> renamedResult)
    {
        logger.info("renameFile starting... , path: {} , newFilename: {}" , path , newFilename);

        Platform.runLater(() ->
        {
            if (FilenameUtils.getName(path).equals(newFilename))
            {
                logger.info("Filename not changed");
                renamedResult.onResult(null);
            }
            else
            {
                final File file = new File(path);
                if (file.exists())
                {
                    final File newFile = new File(String.format("%s%s%s" , file.getParent() , File.separator , newFilename));
                    if (!newFile.exists())
                    {
                        try
                        {
                            final boolean renamed = file.renameTo(newFile);
                            if (renamed)
                            {
                                logger.error("Successfully to rename file: {} : NewFilename: {}" , path , newFilename);
                                renamedResult.onResult(null);
                            }
                            else throw new Exception("Fail rename");
                        }
                        catch (Exception e)
                        {
                            logger.error("Fail to rename file: {} : NewFilename: {}" , path , newFilename);
                            alert(Alert.AlertType.ERROR , "Fail to rename" , e.getMessage() , "Fail to rename this file");
                        }
                    }
                    else
                    {
                        logger.error("New filename is exists: {}" , newFilename);
                        alert(Alert.AlertType.ERROR , "New filename exists" , path , "This new filename exists");
                    }
                }
                else
                {
                    logger.error("File not exists: {}" , path);
                    alert(Alert.AlertType.ERROR , "File not exists" , path , "This file not exists");
                }
            }
        });
    }

    private void update()
    {
        logger.info("Updating: {}" , fileModel);
        final FilesEntity filesEntity = FilesMapper.toFileEntity(fileModel , 0);
        final boolean update = FilesService.update(filesEntity);
        if (update)
        {
            logger.info("Successfully updated file: {}" , fileModel);
            alert(Alert.AlertType.INFORMATION , "Update file" , fileModel.getPath() , "Successfully updated file");

            setValue(fileModel);
        }
        else
        {
            logger.info("Fail update file: {}" , fileModel);
            alert(Alert.AlertType.ERROR , "Error update file" , fileModel.getPath() , "Please check database connection");
        }
    }

    @FXML
    private void onClickBtnRemove()
    {
        logger.info("onClickBtnRemove starting... {}" , fileModel);
        question("Are you want to remove this item from database and directory?" , yesClick ->
                Platform.runLater(() ->
                {
                    final String itemPath = fileModel.getPath();
                    final File itemFile = new File(itemPath);
                    if (itemFile.exists())
                    {
                        try
                        {
                            logger.info("Removing file: {}" , fileModel);
                            final boolean delete = itemFile.delete();
                            if (delete)
                            {
                                logger.info("Successfully to remove file: {}" , fileModel);
                            }
                            else throw new Exception("Fail to delete");
                        }
                        catch (Exception e)
                        {
                            logger.error("Fail to remove file: {}" , fileModel , e);
                            alert(Alert.AlertType.ERROR , "Fail remove" , itemPath , "Error remove file");
                            return;
                        }

                        logger.info("Removing file from database: {}" , fileModel);
                        final boolean deleteFileById = FilesService.deleteFileById(fileModel.getId());

                        if (deleteFileById)
                        {
                            logger.error("Successfully to remove file from database: {}" , fileModel);
                            alert(Alert.AlertType.INFORMATION , "Successfully remove" , itemPath , "Successfully remove file from database");

                            onClickBtnClose();
                        }
                        else
                        {
                            logger.error("Fail to remove file from database: {}" , fileModel);
                            alert(Alert.AlertType.ERROR , "Fail remove" , itemPath , "Error remove file from database");
                        }

                    }
                    else
                    {
                        logger.info("This file not exists: {}" , fileModel);
                        alert(Alert.AlertType.ERROR , "File not exists" , itemPath , "This file not exists");
                    }
                }));
    }

    @FXML
    public void onClickBtnClose()
    {
        logger.info("onClickBtnClose starting...");
        Platform.runLater(() ->
        {
            stage.hide();
            logger.info("Item stage hided");

            stage.close();
            logger.info("Item stage closed");

        });
    }


    private void question(final String message , final Result<?> onClickYes)
    {
        logger.info("question is starting... {}" , message);
        Platform.runLater(() ->
        {
            final Alert alert = new Alert(Alert.AlertType.WARNING , message , ButtonType.YES , ButtonType.CANCEL);

            logger.info("Showing question: {}" , message);
            final Optional<ButtonType> buttonTypeOptional = alert.showAndWait();
            if (buttonTypeOptional.isPresent())
            {
                final ButtonType buttonType = buttonTypeOptional.get();
                logger.info("Question clicked: {} , Message: {}" , buttonType , message);
                if (buttonType.equals(ButtonType.YES)) onClickYes.onResult(null);
            }
        });
    }

    @FXML
    private void onClickBtnOpenDirectoryPath()
    {
        logger.info("onClickBtnOpenDirectoryPath starting...");
        Platform.runLater(() ->
        {
            final String itemPath = fileModel.getPath();

            final File itemFile = new File(itemPath);
            if (itemFile.exists())
            {
                final File parentFile = itemFile.getParentFile();
                logger.error("Opening parent file: {} , Parent: {}" , itemPath , parentFile);

                try
                {
                    Desktop.getDesktop().open(parentFile);
                    logger.info("Opened parent file: {} , Parent: {}" , itemPath , parentFile);
                }
                catch (IOException e)
                {
                    logger.info("Fail to open parent file: {} , Parent: {}" , itemPath , parentFile , e);
                    alert(Alert.AlertType.ERROR , "Error open file" , e.getMessage() , "Error open this file parent");
                }
            }
            else
            {
                logger.error("File not exists: {}" , itemPath);
                alert(Alert.AlertType.ERROR , "File not exists" , itemPath , "This file not exists");
            }
        });
    }

    @FXML
    private void onClickBtnOpenFile()
    {
        logger.info("onClickBtnOpenFile starting...");
        Platform.runLater(() ->
        {
            final String itemPath = fileModel.getPath();

            final File itemFile = new File(itemPath);
            if (itemFile.exists())
            {
                logger.error("Opening file: {}" , itemPath);

                try
                {
                    Desktop.getDesktop().open(itemFile);
                    logger.info("Opened file: {}" , itemPath);
                }
                catch (IOException e)
                {
                    logger.info("Fail to open file: {}" , itemPath , e);
                    alert(Alert.AlertType.ERROR , "Error open file" , e.getMessage() , "Error open this file");
                }
            }
            else
            {
                logger.error("File not exists: {}" , itemPath);
                alert(Alert.AlertType.ERROR , "File not exists" , itemPath , "This file not exists");
            }
        });
    }

    @FXML
    private void onClickBtnMoveFile()
    {
        logger.info("onClickBtnMoveFile starting...");
        Platform.runLater(() ->
        {
            final String itemPath = fileModel.getPath();
            final File itemFile = new File(itemPath);
            if (itemFile.exists())
            {
                final File parentItemFile = itemFile.getParentFile();

                logger.info("Open Directory chooser: {}" , parentItemFile);
                final DirectoryChooser directoryChooser = new DirectoryChooser();
                directoryChooser.setInitialDirectory(parentItemFile);
                final File selectedFile = directoryChooser.showDialog(null);

                logger.info("Selected directory: {}" , selectedFile);
                if (selectedFile != null)
                {
                    if (!selectedFile.getPath().equals(parentItemFile.getPath()))
                    {
                        final String filename = fileModel.getFilename();

                        final File newFile = new File(selectedFile.getPath() + File.separator + filename);
                        final String newFilePath = newFile.getPath();

                        if (!newFile.exists())
                        {
                            logger.info("Moving file: {}" , selectedFile);

                            try
                            {
                                final boolean renamed = itemFile.renameTo(newFile);

                                if (renamed)
                                {
                                    logger.info("Successfully renamed file: {} , Filename: {}" , selectedFile , filename);

                                    fileModel.setPath(newFilePath);
                                    fileModel.setUpdatedAt(LocalDateTime.now().toString());

                                    alert(Alert.AlertType.INFORMATION , "Move file" , selectedFile.getPath() , "Successfully moved file");

                                    update();
                                }
                                else throw new Exception("Fail move");
                            }
                            catch (Exception e)
                            {
                                logger.error("Fail to move file: {} : NewFilename: {} , NewFile: {}" , itemPath , filename , newFile);
                                alert(Alert.AlertType.ERROR , "Fail to rename" , e.getMessage() , "Fail to rename this file");
                            }
                        }
                        else
                        {
                            logger.error("This file name exists in the selected path: {}" , newFilePath);
                            alert(Alert.AlertType.ERROR , "Move file" , filename , "This file name exists in the selected path");
                        }
                    }
                    else
                    {
                        logger.error("A different path was not chosen: {}" , selectedFile);
                        alert(Alert.AlertType.ERROR , "Move file" , itemPath , "A different path was not chosen");
                    }
                }
            }
            else
            {
                logger.error("File not exists: {}" , itemPath);
                alert(Alert.AlertType.ERROR , "File not exists" , itemPath , "This file not exists");
            }
        });
    }
}
