package com.search.files.searchfiles;

import com.search.files.searchfiles.config.DbConfig;
import com.search.files.searchfiles.service.DirectoriesService;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.*;
import java.time.LocalDateTime;

public final class SearchFilesApplication extends Application
{
    private static Connection connection;

    @Override
    public void start(final Stage stage) throws IOException
    {
        final FXMLLoader fxmlLoader = new FXMLLoader(SearchFilesApplication.class.getResource("main.fxml"));
        final Scene scene = new Scene(fxmlLoader.load());
        stage.setTitle("Search files");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args)
    {
        try
        {
            System.out.println("Connecting to database...");
            connection = DbConfig.getConnection();
            System.out.println("Connected to database!");

            launch();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

    }

    public static Connection getDbConnection()
    {
        return connection;
    }
}