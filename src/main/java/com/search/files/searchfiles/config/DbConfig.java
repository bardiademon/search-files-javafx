package com.search.files.searchfiles.config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public final class DbConfig
{
    private final static String USER = "postgres";
    private final static String PASSWORD = "7348";
    private final static String DATABASE_NAME = "search_files";
    private final static String HOST = "localhost";
    private final static int PORT = 5432;

    private DbConfig()
    {
    }

    private static String createUrl()
    {
        return String.format("jdbc:postgresql://%s:%d/%s?sslMode=disable" ,
                HOST , PORT , DATABASE_NAME);
    }

    public static Connection getConnection() throws SQLException
    {
        return DriverManager.getConnection(DbConfig.createUrl() , DbConfig.USER , DbConfig.PASSWORD);
    }
}
