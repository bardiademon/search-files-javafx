package com.search.files.searchfiles.service;

import com.search.files.searchfiles.controller.MainController;
import com.search.files.searchfiles.data.entity.FilesEntity;
import com.search.files.searchfiles.data.mapper.FilesMapper;
import javafx.application.Platform;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

import static com.search.files.searchfiles.SearchFilesApplication.getDbConnection;

public final class FilesService
{
    private final static Logger logger = LogManager.getLogger(MainController.class);

    private FilesService()
    {
    }

    public static void addFiles(final List<FilesEntity> filesEntity , final Result<Boolean> result)
    {
        logger.info("addFiles starting... , {}" , filesEntity);
        Platform.runLater(() ->
        {
            final String query = """
                    insert into "files"("filename","path","description","main_directory_id") values
                    """;
            final StringBuilder values = new StringBuilder();
            for (int i = 0, len = filesEntity.size(); i < len; i++)
            {
                values.append("(?,?,?)");
                if (i + 1 < len) values.append(",");
            }

            logger.info("Executing -> Query: {}" , query);
            try (final PreparedStatement preparedStatement = getDbConnection().prepareStatement(query + "\n" + values , ResultSet.TYPE_SCROLL_SENSITIVE , ResultSet.CONCUR_UPDATABLE))
            {
                int count = 0;
                for (final FilesEntity item : filesEntity)
                {
                    preparedStatement.setString(++count , item.filename());
                    preparedStatement.setString(++count , item.path());
                    preparedStatement.setString(++count , item.description());
                    preparedStatement.setLong(++count , item.mainDirectoryId());
                }

                preparedStatement.execute();

                logger.info("Files added");

                result.onResult(true);
            }
            catch (SQLException e)
            {
                logger.error("Error files add" , e);
                result.onResult(false);
            }
        });
    }

    public static long addFile(final FilesEntity fileEntity)
    {
        logger.info("addFile starting... , {}" , fileEntity);

        final String query = """
                insert into "files"("filename","path","description","main_directory_id") values (?,?,?,?) returning "id"
                """;

        logger.info("Executing -> Query: {}" , query);
        try (final PreparedStatement preparedStatement = getDbConnection().prepareStatement(query , ResultSet.TYPE_SCROLL_SENSITIVE , ResultSet.CONCUR_UPDATABLE))
        {
            System.out.println("preparedStatement.getParameterMetaData() = " + preparedStatement.getParameterMetaData());

            preparedStatement.setString(1 , fileEntity.filename());
            preparedStatement.setString(2 , fileEntity.path());
            preparedStatement.setString(3 , fileEntity.description());
            preparedStatement.setLong(4 , fileEntity.mainDirectoryId());

            try (ResultSet resultSet = preparedStatement.executeQuery())
            {
                logger.info("File added");

                if (resultSet.first()) return resultSet.getLong("id");
            }
        }
        catch (SQLException e)
        {
            logger.error("Error files add" , e);
        }
        return 0;
    }

    public static void findAllFile(final Result<List<FilesEntity>> result)
    {
        logger.info("findAllFile starting... ");
        Platform.runLater(() ->
        {
            final String query = """
                    select * from "files" where "deleted" = ?
                    """;

            logger.info("Executing -> Query: {}" , query);
            try (final PreparedStatement preparedStatement = getDbConnection().prepareStatement(query , ResultSet.TYPE_SCROLL_SENSITIVE , ResultSet.CONCUR_UPDATABLE))
            {
                preparedStatement.setBoolean(1 , false);

                try (final ResultSet resultSet = preparedStatement.executeQuery())
                {
                    final List<FilesEntity> filesEntities = FilesMapper.toFilesEntity(resultSet);
                    result.onResult(filesEntities);
                }
            }
            catch (SQLException e)
            {
                logger.error("Error files add" , e);
                result.onResult(null);
            }
        });
    }

    public static FilesEntity findFileByPath(final String path)
    {
        logger.info("findFileByPath starting... ");

        final String query = """
                select * from "files" where "path" = ? and "deleted" = ?;
                """;

        logger.info("Executing -> Query: {} " , query);
        try (final PreparedStatement preparedStatement = getDbConnection().prepareStatement(query , ResultSet.TYPE_SCROLL_SENSITIVE , ResultSet.CONCUR_UPDATABLE))
        {
            logger.info("Add param");
            preparedStatement.setString(1 , path);
            preparedStatement.setBoolean(2 , false);

            try (final ResultSet resultSet = preparedStatement.executeQuery())
            {
                return FilesMapper.toFileEntity(resultSet);
            }
        }
        catch (SQLException e)
        {
            logger.error("Error files add" , e);
            return null;
        }
    }

    public static void deleteFileByMainDirectory(final long mainDirectoryId , final Result<Boolean> result)
    {
        logger.info("deleteFileByMainDirectory starting...");
        logger.info("deleteFileByPath parameter [mainDirectoryId: {}]" , mainDirectoryId);

        Platform.runLater(() ->
        {
            final String query = """
                    update "files" set "deleted" = ? , "deleted_at" = ? where "main_directory_id" = ?;
                    """;

            logger.info("Executing -> Query: {} " , query);
            try (final PreparedStatement preparedStatement = getDbConnection().prepareStatement(query))
            {
                logger.info("Add param");
                preparedStatement.setBoolean(1 , true);
                preparedStatement.setTimestamp(2 , Timestamp.valueOf(LocalDateTime.now()));
                preparedStatement.setLong(3 , mainDirectoryId);

                final int executeUpdate = preparedStatement.executeUpdate();
                result.onResult(executeUpdate > 0);
            }
            catch (SQLException e)
            {
                logger.error("Error deleteFileByMainDirectory" , e);
                result.onResult(false);
            }
        });
    }

    public static boolean deleteFileById(final long fileId)
    {
        logger.info("deleteFileById starting...");
        logger.info("deleteFileByPath parameter [fileId: {}]" , fileId);

        final String query = """
                update "files" set "deleted" = ? , "deleted_at" = ? where "id" = ?;
                """;

        logger.info("Executing -> Query: {} " , query);
        try (final PreparedStatement preparedStatement = getDbConnection().prepareStatement(query))
        {
            logger.info("Add param");
            preparedStatement.setBoolean(1 , true);
            preparedStatement.setTimestamp(2 , Timestamp.valueOf(LocalDateTime.now()));
            preparedStatement.setLong(3 , fileId);

            final int executeUpdate = preparedStatement.executeUpdate();
            return executeUpdate > 0;
        }
        catch (SQLException e)
        {
            logger.error("Error deleteFileByMainDirectory" , e);
            return false;
        }
    }

    public static void findFileByFilename(final String filename , final Result<List<FilesEntity>> result)
    {
        logger.info("findFileByFilename starting... ");
        Platform.runLater(() ->
        {
            final String query = """
                    select * from "files" where "path" like ? and "deleted" = ?;
                    """;

            logger.info("Executing -> Query: {} " , query);
            try (final PreparedStatement preparedStatement = getDbConnection().prepareStatement(query , ResultSet.TYPE_SCROLL_SENSITIVE , ResultSet.CONCUR_UPDATABLE))
            {
                logger.info("Add param");
                preparedStatement.setString(1 , String.format("%%%s%%" , filename));
                preparedStatement.setBoolean(2 , false);

                try (final ResultSet resultSet = preparedStatement.executeQuery())
                {
                    result.onResult(FilesMapper.toFilesEntity(resultSet));
                }
            }
            catch (SQLException e)
            {
                logger.error("Error findFileByFilename" , e);
                result.onResult(null);
            }
        });
    }

    public static boolean update(final FilesEntity filesEntity)
    {
        logger.info("update starting... {}" , filesEntity);

        final String query = """
                update "files" set "filename" = ? , "path" = ? , "updated_at" = ? , "description" = ? where "id" = ? and "deleted" = ?
                """;

        logger.info("Executing -> Query: {} " , query);
        try (final PreparedStatement preparedStatement = getDbConnection().prepareStatement(query))
        {
            logger.info("preparedStatement");
            int count = 0;
            preparedStatement.setString(++count , filesEntity.filename());
            preparedStatement.setString(++count , filesEntity.path());
            preparedStatement.setTimestamp(++count , Timestamp.valueOf(LocalDateTime.now()));
            preparedStatement.setString(++count , filesEntity.description());
            preparedStatement.setLong(++count , filesEntity.id());
            preparedStatement.setBoolean(++count , false);

            final int executeUpdate = preparedStatement.executeUpdate();
            return executeUpdate > 0;
        }
        catch (SQLException e)
        {
            logger.error("fail delete by id" , e);
            e.printStackTrace();
        }
        return false;
    }
}
