package com.search.files.searchfiles.service;

import com.search.files.searchfiles.data.entity.DirectoriesEntity;
import com.search.files.searchfiles.data.entity.FilesEntity;
import com.search.files.searchfiles.data.mapper.DirectoriesMapper;
import javafx.application.Platform;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.List;

import static com.search.files.searchfiles.SearchFilesApplication.getDbConnection;

public final class DirectoriesService
{
    private final static Logger logger = LogManager.getLogger(DirectoriesService.class);

    private DirectoriesService()
    {
    }

    public static DirectoriesEntity findByPath(final String path)
    {
        final String query = """
                select * from "directories" where "path" = ? and deleted = ?;
                """;

        logger.info("Executing -> Query: {}" , query);
        try (final PreparedStatement preparedStatement = getDbConnection().prepareStatement(query , ResultSet.TYPE_SCROLL_SENSITIVE , ResultSet.CONCUR_UPDATABLE))
        {
            logger.info("preparedStatement");
            preparedStatement.setString(1 , path);
            preparedStatement.setBoolean(2 , false);

            try (final ResultSet resultSet = preparedStatement.executeQuery())
            {
                return DirectoriesMapper.toDirectoryEntity(resultSet);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public static void findAll(final Result<List<DirectoriesEntity>> result)
    {
        Platform.runLater(() ->
        {
            final String query = """
                    select * from "directories" where deleted = ?;
                    """;

            logger.info("Executing -> Query: {}" , query);
            try (final PreparedStatement preparedStatement = getDbConnection().prepareStatement(query , ResultSet.TYPE_SCROLL_SENSITIVE , ResultSet.CONCUR_UPDATABLE))
            {
                logger.info("preparedStatement");
                preparedStatement.setBoolean(1 , false);

                try (final ResultSet resultSet = preparedStatement.executeQuery())
                {
                    result.onResult(DirectoriesMapper.toDirectoriesEntity(resultSet));
                    return;
                }
            }
            catch (SQLException e)
            {
                e.printStackTrace();
            }
            result.onResult(null);
        });
    }

    public static boolean add(final String path)
    {
        logger.info("add starting...");

        final String query = """
                insert into "directories"("path") values (?) returning "id";
                """;

        logger.info("Executing -> Query: {} " , query);
        try (final PreparedStatement preparedStatement = getDbConnection().prepareStatement(query , ResultSet.TYPE_SCROLL_SENSITIVE , ResultSet.CONCUR_UPDATABLE))
        {
            preparedStatement.setString(1 , path);

            try (final ResultSet resultSet = preparedStatement.executeQuery())
            {
                final Long id = DirectoriesMapper.getId(resultSet);
                return id != null && id > 0;
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean deleteById(final long id)
    {
        logger.info("deleteById starting... {}" , id);

        final String query = """
                update "directories" set "deleted" = ? , "deleted_at" = ? where "id" = ?;
                """;

        logger.info("Executing -> Query: {} " , query);
        try (final PreparedStatement preparedStatement = getDbConnection().prepareStatement(query))
        {
            logger.info("preparedStatement");
            preparedStatement.setBoolean(1 , true);
            preparedStatement.setTimestamp(2 , Timestamp.valueOf(LocalDateTime.now()));
            preparedStatement.setLong(3 , id);

            final int executeUpdate = preparedStatement.executeUpdate();
            return executeUpdate > 0;
        }
        catch (SQLException e)
        {
            logger.error("fail delete by id" , e);
            e.printStackTrace();
        }
        return false;
    }
}
