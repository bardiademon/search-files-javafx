package com.search.files.searchfiles.service;

public interface Result<T>
{
    void onResult(T t);
}
