-- for search
create table if not exists "directories"
(
    "id"             bigserial primary key not null,
    "path"           text                  not null,
    "created_at"     timestamp             not null default current_timestamp,
    "last_search_at" timestamp             null,
    "deleted"        boolean               not null default false,
    "deleted_at"     timestamp             null
);

create table if not exists "files"
(
    "id"                bigserial primary key not null,
    "filename"          text                  not null,
    "path"              text                  not null,
    "description"       text                  null,
    "updated_at"        timestamp             null,
    "created_at"        timestamp             not null default current_timestamp,
    "deleted"           boolean               not null default false,
    "deleted_at"        timestamp             null,
    "main_directory_id" bigint                not null
);

alter table "files"
    add foreign key ("main_directory_id") references "directories" ("id");